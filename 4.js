var numbers = [1,5,7,4,32,54,75,31,32,8];

function filterRange (array, a, b) {
  var result = [];
  for(item in array) {
    if(array[item] >= a && array[item] <= b) {
      result.push(array[item]);
    }
  }
  return result;
}

console.log(filterRange(numbers, 5, 8));
