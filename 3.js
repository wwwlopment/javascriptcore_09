var styles = ['Джаз', 'Блюз','Рок-н-ролл','Класика'];


function find(arr, value){
  for (i in arr) {
    if(value == arr[i]){
      return i;
    }
  }
  return -1;
}

console.log(find(styles, 2));

console.log(find(styles, 'Класика'));
