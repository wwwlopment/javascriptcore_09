function camelize (str) {
  var arr = str.split('-');
  var result = [];
  for (item in arr) {
    if(item != 0){
      result.push(arr[item].replace(/^./, str => str.toUpperCase()));
    } else {
      result.push(arr[item]);
    }
  }
  return result.join('');
}

console.log(camelize('my-short-string'));
